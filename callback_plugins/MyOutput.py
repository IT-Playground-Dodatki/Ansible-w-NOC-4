# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)

__metaclass__ = type

from ansible.plugins.callback import CallbackBase
from jinja2 import Environment, FileSystemLoader
from pprint import pprint
import json


class CallbackModule(CallbackBase):
    CALLBACK_VERSION = 2.0
    CALLBACK_NAME = 'MyOutput'
    CALLBACK_NEEDS_WHITELIST = False
    # If PRINT_DEBUG = True then additional debug output will show on console
    PRINT_DEBUG = False  # type: bool

    def __init__(self, display=None):
        """
        Overloaded constructor of CallbackModule class. It will call constructor from parent
        :param display:
        """
        # Call the __init__ from parent class
        super(CallbackModule, self).__init__(display=display)
        # Define variable to store playbook variables we will use later in jinja2 template
        self.jinja_output_var = dict()

    def v2_runner_on_ok(self, result):
        """
        Overloaded function run after successful task execution
        :param result:
        :type result: result
        :return:
        """
        super(CallbackModule, self).v2_runner_on_ok(result)
        if self.PRINT_DEBUG:
            pprint("--- runner --->")
            pprint(result._result)
            pprint("<-- runner ---")

        if 'ansible_facts' not in result._result:
            return
        if 'template_vars' in result._result['ansible_facts']:
            # self.jinja_output_var[result._host.get_name().encode('ascii', 'ignore')] = {}
            # self.jinja_output_var[result._host.get_name().encode('ascii', 'ignore')] = result._result['ansible_facts']['template_vars'].encode('ascii', 'ignore')
            self.jinja_output_var[result._host.get_name()] = {}
            self.jinja_output_var[result._host.get_name()] = result._result['ansible_facts']['template_vars']

        if self.PRINT_DEBUG:
            pprint("--- self.jinja_output_var -->")
            pprint(self.jinja_output_var)
            pprint("<-- self.jinja_output_var ---")

    def v2_playbook_on_stats(self, stats):
        """ Executed at playbook summary (last task). It will call function
            that will create output to stdout using jinja2 template. Then we
            call original function from the Class

        :rtype: object
        """
        json_var = json.dumps(self.jinja_output_var)
        if self.PRINT_DEBUG:
            pprint("--- json_var as passed to jinja2 render -->")
            pprint(json_var)
            pprint("<-- json_var as passed to jinja2 render ---")

        env = Environment(
            # Select folder with templates
            loader=FileSystemLoader('./templates'),
            # If trim_blocks is True then jinja2 will remove white lines after block
            trim_blocks=True,
            # Set autoescape=True to mitigate XSS vulnerabilities
            autoescape=True
            # loader = PackageLoader('NOC-Summary','templates')
        )
        template = env.get_template('Podsumowanie.j2')
        print(template.render(template_variables=json.loads(json_var)))
        super(CallbackModule, self).v2_playbook_on_stats(stats)

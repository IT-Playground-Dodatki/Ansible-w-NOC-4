#!/usr/bin/python


class FilterModule(object):

    def filters(self):
        return {
            'extract_ge_if': self.extract_ge_if
        }

    def extract_ge_if(self, data):
        data_return = []
        for if_name in data['ansible_facts']['ansible_net_interfaces']:
            if (data['ansible_facts']['ansible_net_interfaces'][if_name]['oper-status'] == "down" and
                    "ge-" in if_name):
                data_return.append(if_name)
        return data_return
